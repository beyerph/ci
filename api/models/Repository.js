/**
* Repository.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'repositories',
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
    },

    projects: {
      collection: 'project',
      via: 'repositories'
    },

    sort: {
      type: 'integer'
    },
    name: {
      type: 'string',
    },
    url: {
      type: 'string',
    },
    relativePath: {
      type: 'string',
    },
    createdAt: {
      type: 'datetime',
    },
    updatedAt: {
      type: 'datetime'
    }
  }
};

