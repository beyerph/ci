/**
* Project.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'projects',
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
    },

    repositories:{
      collection: 'repository',
      via: 'projects'
    },

    name: {
      type: 'string',
    },
    createdAt: {
      type: 'datetime',
    },
    updatedAt: {
      type: 'datetime'
    }
  }

};

