/**
* Environment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'environments',
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
    },
    sort: {
      type: 'integer'
    },
    name: {
      type: 'string',
    },
    absolutePath: {
      type: 'string',
    },
    branch: {
      type: 'string',
    },
    remote: {
      type: 'string',
    },
    createdAt: {
      type: 'datetime',
    },
    updatedAt: {
      type: 'datetime'
    }
  }
};

