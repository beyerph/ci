/**
 * ProjectController
 *
 * @description :: Server-side logic for managing projects
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


var Client = require('ssh2').Client;


module.exports = {
  detail: function (req, res) {
    var detailId = req.param('id');

    if(!detailId){
      res.status(404);
      res.view('404', {message: 'Sorry, you need to tell us the ID of the Project you want!'});
    }



    console.log("REQUEST");
    res.view('project/detail', {message: 'Sorry, you need to tell us the ID of the Project you want!'});

  },


  detailGitBash: function (req, res) {

    if (!req.isSocket) {
      return res.badRequest();
    }


    var detailId = req.param('id');

    if(!detailId){
      res.status(404);
      res.view('404', {message: 'Sorry, you need to tell us the ID of the Project you want!'});
    }


    var socketId = sails.sockets.id(req.socket);








    var conn = new Client();
    conn.on('ready', function() {
      console.log("# " + req.param('exec') + "\n");
      sails.sockets.emit(socketId, 'console', "# " + req.param('exec') + "\n");
      conn.exec(req.param('exec'), function(err, stream) {
        if (err) throw err;
        stream.on('close', function(code, signal) {
          console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
          sails.sockets.emit(socketId, 'console', 'Stream :: close :: code: ' + code + ', signal: ' + signal);
          conn.end();
        }).on('data', function(data) {
          sails.sockets.emit(socketId, 'console', data.toString());
        }).stderr.on('data', function(data) {
          sails.sockets.emit(socketId, 'console', data.toString());
        });
      });
    }).connect({
      host: 'localhost',
      port: 22,
      username: 'vagrant',
      password: 'vagrant'
      //privateKey: require('fs').readFileSync('/here/is/my/key')
    });


  },


};















