io.socket.on("blaaaaa", function(data){
  console.error(io.socket.on("<model identity>", listenerFunction));
})



var data = {
  "id": 1,
  "name": "Servicecard",
  "created_at": "2016-01-10 16:00:00",
  "updated_at": "2016-01-10 16:00:00",


  "repositories": [{
    "id": 1,
    "project_id": 1,
    "sort": 1,
    "name": "MyNews",
    "url": "https://github.com/beyerph/MyNews.git",
    "relative_path": "/MyNews",
    "created_at": "2015-01-10 00:00:00",
    "updated_at": "2015-01-10 00:00:00",
  }],


  "status": [{
    "envId": 1,
    "repoId": 1,
    "behing": 1,
    "ahead": 2,
    "branches": ["develop", "master"],
    "branch": "master",
    "missing": true
  },{
    "envId": 2,
    "repoId": 1,
    "behing": 1,
    "ahead": 2,
    "branches": ["develop", "master"],
    "branch": "master",
    "missing": true
  },{
    "envId": 3,
    "repoId": 1,
    "behing": 1,
    "ahead": 2,
    "branches": ["develop", "master"],
    "branch": "master",
    "missing": false
  }],

  "environments": [{
    "id": 1,
    "project_id": 1,
    "sort": 1,
    "name": "DEV",
    "absolute_path": "/home/vagrant/ci-tool/test/dev",
    "branch": "develop",
    "remote": "local",
    "created_at": "2015-01-10 00:00:00",
    "updated_at": "2015-01-10 00:00:00"
  }, {
    "id": 2,
    "project_id": 1,
    "sort": 1,
    "name": "STAGE",
    "absolute_path": "/home/vagrant/ci-tool/test/stage",
    "branch": "develop",
    "remote": "local",
    "created_at": "2015-01-10 00:00:00",
    "updated_at": "2015-01-10 00:00:00"
  }, {
    "id": 3,
    "project_id": 1,
    "sort": 1,
    "name": "LIVE",
    "absolute_path": "/home/vagrant/ci-tool/test/live",
    "branch": "develop",
    "remote": "local",
    "created_at": "2015-01-10 00:00:00",
    "updated_at": "2015-01-10 00:00:00"
  }]
};

io.socket.on('connect', function(){ console.log("Websocket connected"); });

$(function(){

  $('[data-toggle="popover"]').popover({html:true});


  // Console
  setInterval(function(){
    var element = document.getElementById("result");
    element.scrollTop = element.scrollHeight;
  },10);



  Vue.config.debug = true;
  var project = new Vue({
    el: 'body',
    socket: null,
    data: {
      project: data,
      console: ''
    },
    ready: function(){
      var v = this;

      //v.socket = io.socket.get(window.location.href);

      io.socket.on('connect', function(){ console.log("Websocket connected"); });
      io.socket.on('error', function(data){ console.error(data); });
      io.socket.on('disconnect', function(){ console.log("Websocket disconnect"); });

      io.socket.on('console', function(data) {
        v.console += data+ "\n";
      });

    },
    methods: {

      onGitAction: function(e){
        var command = $(e.target).attr('data-exec');
        this.console = "";
        io.socket.post('/project/detailGitBash/1', { exec: command }, function (resData) {
          console.log(resData);
        });
      },

      getEnvironments: function(repoId){
        var v = this;
        var re = {};
        jQuery.extend(re, v.project.environments);

        $.each(v.project.environments, function(ekey, environment){

          $.each(v.project.status, function(skey, status){
            if(status.envId==environment.id && status.repoId==repoId ){
              jQuery.extend(re[ekey], {"status": status});
            }
          });
        });
        return re;
      }


    }
  });

});


